from pre import *

def getFrequency(message):
    counts = dict()
    #words = message.split()

    for word in message:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1

    return counts

def getVocabulary(trainningSet):

    vocabulary = []
    for m in trainningSet:
        voc_f = getFrequency(m[1])
        for x in voc_f:
            vocabulary.append(x)
    return vocabulary

def getEachMessagesFreq(dataset):
    messages_freq = dict()
    i=0
    for message in dataset:
            messages_freq[i]=[message[0],getFrequency(message[1])]
            i+=1
    return messages_freq

def spamProbability(trainningSet):
    c=0
    for m in trainningSet:
        if m[0]:
            c+=1
    return c/len(trainningSet)

def wordSpamProbability(word,trainningSet):
    s=0
    wc=0
    vocabulary = getVocabulary(trainningSet)
    if word in vocabulary:
        l = getEachMessagesFreq(trainningSet)
        for x in l:
            if l[x][0]:
                s+=1
                if word in l[x][1]:
                    wc+=1
        return wc/s
    else:
        return -1

def wordNotSpamProbability(word,trainningSet):
    s=0
    wc=0
    vocabulary = getVocabulary(trainningSet)
    if word in vocabulary:
        l = getEachMessagesFreq(trainningSet)
        for x in l:
            if not l[x][0]:
                s+=1
                if word in l[x][1]:

                    wc+=1
        return wc/s
    else:
        return -1

def isSpam(trainningSet,message):
    pSpam = spamProbability(trainningSet)
    vocabulary = getVocabulary(trainningSet)
    messageWords = getFrequency(message)
    paramSpam = pSpam
    paramNotSpam = 1 - pSpam
    for word in messageWords:
        if word in vocabulary:
            pS = wordSpamProbability(word,trainningSet)
            pNs = wordNotSpamProbability(word,trainningSet)
            #print("chosen attribute ->"+str(word))
            #print("probability of '"+str(word) +"' to be in the message knowing that the message is a spam ->"+str(pS))
            #print("probability of '"+str(word) +"' to be in the message knowing that the message is NOT a spam ->"+str(pNs))
            if pS > 0 :
                paramSpam *= pS
            else:
                if pS == 0 and pNs >0:
                    paramSpam *=0.0001

            if pNs > 0 :
                paramNotSpam *= pNs
            else:
                if pNs == 0 and pS >0:
                    paramNotSpam *=0.0001
                #print("attribute "+str(word)+" is removed because trainningSet is not complete enough for this attribute")
    return paramSpam/paramNotSpam

def accuracy(TP,TN,FP,FN):
    return (TP+TN)/(TP+FP+FN+TN)

def precision(TP,TN,FP,FN):
    return TP/(TP+FP)

def recall(TP,TN,FP,FN):
    return TP/(TP+FN)

def f1Score(TP,TN,FP,FN):
    r = recall(TP,TN,FP,FN)
    p = precision(TP,TN,FP,FN)
    return 2*(r*p)/(r+p)

def classify(trainningSet,testSet,alpha):
    TP=0
    TN=0
    FP=0
    FN=0

    for e in testSet:
        prediction = isSpam(trainningSet,e[1])
        print("The message : '"+str(e[1])+"' is a spam "+ str(e[0])+" and the program predicated "+str(prediction > alpha))
        if prediction > alpha:#we here predict that the message is a spam
            if e[0]:#the message is an actual spam
                TP+=1
            else:
                FP+=1
        else:
            if e[0]:#the message is an actual spam
                FN+=1
            else:
                TN+=1

    print("Accuracy : "+str(accuracy(TP,TN,FP,FN)))
    print("Recall : "+str(recall(TP,TN,FP,FN)))
    print("Precision : "+str(precision(TP,TN,FP,FN)))
    print("F1 : "+str(f1Score(TP,TN,FP,FN)))


trainningSet = preprocess("training_dataset.txt")
testSet = preprocess("test_dataset.txt")

classify(trainningSet,testSet,1)
