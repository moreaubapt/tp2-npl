import nltk
import re
import string
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

def preprocess(file):
    mon_fichier = open(file, "r")
    sms = mon_fichier.read()
    mon_fichier.close()
    sms = sms.splitlines()
    stop_words = set(stopwords.words('english'))
    stemmer= PorterStemmer()
    result = []
    for l in sms:
        l = l.split("\t")
        l[1] = l[1].lower()
        l[1] = re.sub(r'\d+', '', l[1])
        l[1] = l[1].translate(str.maketrans('','',string.punctuation))
        tokens = word_tokenize(l[1])
        l[1] = [i for i in tokens if not i in stop_words]
        for word in l[1]:
            word = stemmer.stem(word)
        if l[0] == "spam":
            result.append([True,l[1]])
        else:
            result.append([False,l[1]])
    return result
